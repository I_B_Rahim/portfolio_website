var findForm = document.getElementsByTagName('form');
window.onload = function(){
	$('#commentError').hide();
	findForm[0].onsubmit = validateForm;
	
	$(document).ready(function(){
		$('[data-toggle="popover"]').popover();
	});
};

function validateForm() {
	if (findForm[0].myMessage.value.length < 10) {
		var errorMessage = document.getElementById('commentError');
		errorMessage.innerHTML = "Please enter more messages";
		$('#commentError').show();
		return false;
	}
	return true;
};
